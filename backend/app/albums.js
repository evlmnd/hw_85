const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const Album = require('../models/Album');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.artist) {
        Album.find({artist: req.query.artist})
            .populate('artist')
            .sort({year: 1})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            });
    } else {
        Album.find({publish: true})
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    }
});

// router.get('/:id', (req, res) => {
//     Album.find({_id: req.params.id})
//         .then(result => {
//             if (result) res.send(result);
//             else res.sendStatus(404);
//         })
//         .catch(() => res.sendStatus(500));
// });

router.get('/admin', [auth, permit('admin')], async(req, res) =>{
    try {
        const results = await Album.find();
        if (results) res.send(results);
    } catch (error) {
        res.status(500).send({error: error});
    }
});

router.get("/:id", (req, res) => {
    const id = req.params.id;
    Album.find({userId: id})
        .then(result => {
            if (result) res.send(result);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});


router.post('/', [auth, upload.single('photo')], (req, res) => {
    const data = req.body;

    if (req.file) {
        data.photo = req.file.filename;
    } else {
        data.photo = null;
    }

    const album = new Album(data);
    album
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send({error: error}));
});

module.exports = router;
