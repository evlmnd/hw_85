const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', async (req, res) => {
    const token = req.get('Token');
    if (!token) res.status(401).send({message: 'No token here'});

    const user = await User.findOne({token: token});
    if (user) {
        const trackHistory = new TrackHistory({
            user: user._id,
            track: req.body._id,
            dateTime: new Date()
        });

        await trackHistory.save();

        res.send(trackHistory);
    } else {
        res.status(401).send({message: 'Unauthorized'});
    }
});

router.get('/', auth, async (req, res) => {
    const user = await User.findOne({token: req.get('Token')});
    TrackHistory.find({user: user._id})
        .sort({datetime: -1}).populate({path: 'track', populate: {path: 'album', populate: {path: 'artist'}}})
        .then(result => {
            if (result) res.send(result);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});


module.exports = router;