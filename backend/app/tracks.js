const express = require('express');
const Track = require('../models/Track');
const auth = require('../middleware/auth');


const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.album) {
        Track.find({album: req.query.album})
            .populate('album')
            .sort({position: 1})
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            });
    } else {
        Track.find({publish: true})
            .then(results => res.send(results))
            .catch(() => res.sendStatus(500));
    }
});


router.post('/', auth, (req, res) => {
    const data = req.body;
    const track = new Track(data);
    track
        .save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send({error: error}));
});

module.exports = router;
