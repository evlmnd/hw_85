const express = require('express');
const axios = require('axios');
const nanoid = require('nanoid');

const config = require('../config');
const auth = require('../middleware/auth');
const User = require('../models/User');


const router = express.Router();

router.get('/', async (req, res) => {
    const users = await User.find();
    res.send(users);
});

router.post('/', (req, res) => {
    const user = new User({
        username: req.body.username,
        password: req.body.password,
        displayName: req.body.name,
        avatar: req.body.avatar
    });

    user.createToken();

    user.save()
        .then(user => res.send({message: 'User registered', user}))
        .catch(error => res.status(400).send(error))
});

router.post('/facebookLogin', async (req, res) => {

    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
    const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;
    console.log(debugTokenUrl);
    try {
        const response = await axios.get(debugTokenUrl);

        const responseData = response.data;


        if (responseData.data.error) {
            return res.status(500).send({error: 'Token incorrect'});
        }

        if (responseData.data.user_id !== req.body.id) {
            return res.status(500).send({error: 'User is wrong'});
        }

        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                username: req.body.email || req.body.id,
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
                avatar: req.body.picture.data.url
            });
        }

        user.createToken();
        await user.save();
        console.log(user);

        return res.send({message: 'Login or register successful!', user});
    } catch (e) {
        return res.status(500).send({error: 'Something went wrong'});
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: 'Username not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password is wrong'});
    }

    user.createToken();

    await user.save();

    return res.send({message: 'Login successful', user});
});


router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};

    if (!token) {
        return res.send(success);
    }
    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }

    user.createToken();
    await user.save();

    return res.send(success);
});

module.exports = router;
