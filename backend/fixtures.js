const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users = await User.create({
        username: 'user1', password: '1'
    }, {
        username: 'admin1', password: '1'
    });

    const artist = await Artist.create(
        {name: "TR/ST", photo: "trst.jpg", info: "trst info"},
        {name: "Queen", photo: "queen.jpg", info: "queen info"},
        {name: "Stephan Bodzin", photo: "stephan bodzin.jpg", info: "stephan info"},
    );

    const albums = await Album.create(
        {name: "Destroyer-1", year: 2019, artist: artist[0]._id, photo: "Destroyer-1.jpg"},
        {name: "Joyland", year: 2014, artist: artist[0]._id, photo: "Joyland.jpg"},
        {name: "A Night at the Opera", year: 1975, artist: artist[1]._id, photo: "A Night at the Opera.jpg"},
        {name: "Powers Of Ten", year: 2015, artist: artist[2]._id, photo: "Powers Of Ten.jpg"},
    );

    const tracks = await Track.create(
        {position: 2, name: "Unbleached", album: albums[0]._id, duration: "03:45"},
        {position: 1, name: "Gone", album: albums[0]._id, duration: "03:48"},
        {position: 1, name: "Slightly Floating", album: albums[1]._id, duration: "03:35"},
        {position: 1, name: "Love of my life", album: albums[2]._id, duration: "03:48"},
        {position: 2, name: "Bohemian Rhapsody", album: albums[2]._id, duration: "06:48"},
        {position: 1, name: "Birth", album: albums[3]._id, duration: "07:53"},
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});
