const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
    position: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    album: {
        type: Schema.Types.ObjectId,
        ref: 'Album',
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    publish: {
        type: Boolean,
        default: false
    },
    userId: {
        type: Schema.Types.ObjectId
    }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;
