import React, {Component, Fragment} from 'react';
import {Route, Switch, withRouter} from "react-router-dom";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import ArtistsList from "./containers/ArtistList/ArtistsList";
import AlbumsList from "./containers/AlbumsList/AlbumsList";
import TrackList from "./containers/TrackList/TrackList";
import {Container} from "reactstrap";
import {connect} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {logoutUser} from "./store/actions/usersActions";
import TrackHistory from "./containers/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";

class App extends Component {
  render() {
    return (
        <Fragment>
            <header>
                <Toolbar
                    user={this.props.user}
                    logout={this.props.logoutUser}
                />
            </header>
            <Container style={{marginTop: '20px'}}>
                <Switch>
                    <Route path="/" exact component={ArtistsList} />
                    <Route path="/albums" exact component={AlbumsList} />
                    <Route path="/tracks" exact component={TrackList} />
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/track_history" exact component={TrackHistory}/>
                    <Route path="/artists/add_artist" exact component={AddArtist}/>
                    <Route path="/albums/add_album" exact component={AddAlbum}/>
                    <Route path="/tracks/add_track" exact component={AddTrack}/>
                </Switch>
            </Container>
        </Fragment>
    );
  }
}


const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

