import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";

const UserMenu = ({user, logout}) => (
    <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            Hello, {user.user.displayName}
        </DropdownToggle>
        {/*<img src={user.user.avatar} alt=""/>*/}

        <DropdownMenu right>
            <DropdownItem>
                Show profile
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem onClick={logout}>
                Logout
            </DropdownItem>
        </DropdownMenu>
    </UncontrolledDropdown>
);

export default UserMenu;