import React from 'react';
import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavItem,
    NavLink, UncontrolledDropdown
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="light" light expand="md">

            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>Artists</NavLink>
                </NavItem>
                {user ?
                    <NavItem><NavLink tag={RouterNavLink} to="/track_history" exact>Track History</NavLink></NavItem>
                    : null}

                {user ? (
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                Add
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>
                                    <NavItem>
                                        <NavLink tag={RouterNavLink} to="/artists/add_artist" exact>Add artist</NavLink>
                                    </NavItem>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavItem>
                                        <NavLink tag={RouterNavLink} to="/albums/add_album" exact>Add album</NavLink>
                                    </NavItem>
                                </DropdownItem>
                                <DropdownItem>
                                    <NavItem>
                                        <NavLink tag={RouterNavLink} to="/tracks/add_track" exact>Add track</NavLink>
                                    </NavItem>
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    )
                    : null}

                {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
                {user ? <img src={user.user.avatar} alt=""/> : null}
            </Nav>
        </Navbar>
    );
};


export default Toolbar;
