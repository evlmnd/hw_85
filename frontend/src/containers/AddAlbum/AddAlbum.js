import React, {Component} from "react";
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {fetchArtistsList} from "../../store/actions/artistsActions";
import {addAlbum} from "../../store/actions/albumsActions";

class AddAlbum extends Component {

    state = {
        name: '',
        year: '',
        photo: '',
        artist: '',
        userId: this.props.user._id
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    componentDidMount() {
        this.props.fetchArtistsList();
    }

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>

                <FormElement
                    propertyName="artist"
                    title="Artist"
                    type="select"
                    value={this.state.artist}
                    onChange={this.inputChangeHandler}
                    required
                >
                    <option value="">Please select artist</option>
                    {this.props.artists.map(artist => (
                        <option key={artist._id} value={artist._id}>{artist.name}</option>
                    ))}
                </FormElement>

                <FormElement
                    propertyName="name"
                    title="Name"
                    type="text"
                    value={this.state.name}
                    onChange={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="year"
                    title="Year"
                    type="number"
                    value={this.state.year}
                    onChange={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="photo"
                    title="Photo"
                    type="file"
                    onChange={this.fileChangeHandler}
                />

                <FormGroup>
                    <Col sm={10}>
                        <Button type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user.user,
        artists: state.artists.artists
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchArtistsList: () => dispatch(fetchArtistsList()),
        onSubmit: albumData => dispatch(addAlbum(albumData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);