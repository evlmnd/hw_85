import React, {Component} from 'react';
import { connect } from "react-redux";
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {addArtist} from "../../store/actions/artistsActions";

class AddArtist extends Component {
    state = {
        name: '',
        info: '',
        photo: '',
        userId: this.props.user._id
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>

                <FormElement
                    propertyName="name"
                    title="Name"
                    type="text"
                    value={this.state.name}
                    onChange={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="info"
                    title="About"
                    type="textarea"
                    value={this.state.info}
                    onChange={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="photo"
                    title="Photo"
                    type="file"
                    onChange={this.fileChangeHandler}
                />

                <FormGroup>
                    <Col sm={10}>
                        <Button type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onSubmit: artistData => dispatch(addArtist(artistData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddArtist);
