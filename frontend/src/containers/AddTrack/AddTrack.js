import React, { Component } from "react";
import { connect } from "react-redux";
import {fetchAlbumsList} from "../../store/actions/albumsActions";
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {addTrack} from "../../store/actions/tracksActions";

class AddTrack extends Component {

    state = {
        position: '',
        name: '',
        album: '',
        duration: '',
        userId: this.props.user._id
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });


        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    componentDidMount() {
        this.props.getAlbumsList('/albums');
    }

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>

                <FormElement
                    propertyName="album"
                    title="Album"
                    type="select"
                    value={this.state.album}
                    onChange={this.inputChangeHandler}
                    required
                >
                    <option value="">Please select album</option>
                    {this.props.albums.map(album => (
                        <option key={album._id} value={album._id}>{album.name}</option>
                    ))}
                </FormElement>

                <FormElement
                    propertyName="position"
                    title="Number"
                    type="number"
                    value={this.state.position}
                    onChange={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="name"
                    title="Name"
                    type="text"
                    value={this.state.name}
                    onChange={this.inputChangeHandler}
                    required
                />

                <FormElement
                    propertyName="duration"
                    title="Duration"
                    type="text"
                    value={this.state.duration}
                    onChange={this.inputChangeHandler}
                    required
                />

                <FormGroup>
                    <Col sm={10}>
                        <Button type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums,
        user: state.users.user.user
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getAlbumsList: (query) => dispatch(fetchAlbumsList(query)),
        onSubmit: trackData => dispatch(addTrack(trackData))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddTrack);