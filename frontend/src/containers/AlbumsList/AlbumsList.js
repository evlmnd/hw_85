import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";
import CardItem from "../../components/CardItem/CardItem";
import {CardDeck, Col} from "reactstrap";
import config from "../../config";
import {fetchAlbumsList, getAlbumsListForAdmin, getMyAlbumsList} from "../../store/actions/albumsActions";

class AlbumsList extends Component {

    getAlbumInfo = id => {
        this.props.history.push('/tracks?album=' + id);
    };

    componentDidMount() {
        // this.props.fetchAlbumsList(this.props.location.pathname + this.props.location.search);
        if (this.props.location.pathname === `/my-albums/${this.props.match.params.id}` ) {
            this.props.onGetMyAlbumsList(this.props.match.params.id);
        }

        if (this.props.location.pathname === '/albums') {
            if (this.props.user && this.props.user.role === 'admin') {
                this.props.getAlbumsListForAdmin();
            } else {
                this.props.fetchAlbumsList(this.props.location.pathname + this.props.location.search);
            }
        }
    };

    render() {
        let albums;
        if (this.props.albums.length > 0) {
            albums = this.props.albums.map(item => {
                return <Col sm={3} key={item._id}>
                    <CardItem
                        title={item.name}
                        subtitle={item.year.toString()}
                        click={() => this.getAlbumInfo(item._id)}
                        imageSrc={config.apiUrl + '/uploads/' + item.photo}
                    />
                </Col>
            });
        } else {
            albums = <h3>Nothing here yet</h3>
        }


        return (
            <Fragment>
                <h1>{this.props.artistName}</h1>
                <CardDeck>
                    {albums}
                </CardDeck>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        artistName: state.albums.name,
        albums: state.albums.albums,
        user: state.users.user.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchAlbumsList: query => dispatch(fetchAlbumsList(query)),
        getAlbumsListForAdmin: () => dispatch(getAlbumsListForAdmin()),
        onGetMyAlbumsList: id => dispatch(getMyAlbumsList(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumsList);