import React, {Component, Fragment} from 'react';
import CardItem from "../../components/CardItem/CardItem";
import {connect} from "react-redux";
import {fetchArtistsList, getArtistsListForAdmin, getMyArtistsList} from "../../store/actions/artistsActions";
import {CardDeck, Col} from "reactstrap";
import config from "../../config";


class ArtistsList extends Component {

    onGetAlbums = (id) => {
        this.props.history.push('/albums?artist=' + id);
    };

    componentDidMount() {
        // if (this.props.user) {
        //     this.props.getArtistsListForAdmin();
        // } else {
            this.props.fetchArtistsList();
        // }

        if ( this.props.location.pathname === `/my-artists/${this.props.match.params.id}` ) {
            this.props.getMyArtistsList(this.props.match.params.id);
        }
    };

    render() {
        const artists = this.props.artists.map(item => {
            return <Col sm={3} key={item._id}>
                <CardItem
                    title={item.name}
                    click={() => this.onGetAlbums(item._id)}
                    imageSrc={config.apiUrl + '/uploads/' + item.photo}
                />
            </Col>
        });

        return (
            <Fragment>
                <h1>Artists</h1>
                <CardDeck>
                    {artists}
                </CardDeck>
            </Fragment>

        );
    }
}


const mapStateToProps = state => {
    return {
        artists: state.artists.artists,
        setArtist: state.artists.setArtist,
        user: state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchArtistsList: () => dispatch(fetchArtistsList()),
        getArtistsListForAdmin: () => dispatch(getArtistsListForAdmin()),
        getMyArtistsList: id => dispatch(getMyArtistsList(id))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsList);