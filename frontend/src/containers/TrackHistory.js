import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {getTrackHistory} from "../store/actions/trackHistoryActions";
import {CardDeck, Col} from "reactstrap";
import CardItem from "../components/CardItem/CardItem";
import {Redirect} from "react-router-dom";

class TrackHistory extends Component {

    componentDidMount() {
        if (this.props.user) this.props.getTrackHistory(this.props.user.token);
    };

    render() {
        const trackHistory = (
            this.props.history && this.props.history.map(item => {
                return <Col sm={12} key={item._id}>
                    <CardItem
                        title={item.track.album.artist.name +
                        " - " +
                        item.track.name +
                        " ( " +
                        item.track.album.name +
                        " ) "}
                    />
                </Col>
            })
        );

        return (
            <Fragment>
                track history here
                <CardDeck>
                    {this.props.user ? trackHistory : <Redirect to="/login"/>}
                </CardDeck>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        history: state.trackHistory.history
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getTrackHistory: token => dispatch(getTrackHistory(token))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);
