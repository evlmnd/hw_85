import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";
import {fetchTrackList} from "../../store/actions/tracksActions";
import {CardDeck, Col} from "reactstrap";
import CardItem from "../../components/CardItem/CardItem";
import {postTrackToHistory} from "../../store/actions/trackHistoryActions";

class TrackList extends Component {

    clickTrack = (id) => {
        if (this.props.user) {
            this.props.postTrackToHistory(id, this.props.user.token);
        }
    };

    componentDidMount() {
        this.props.fetchTrackList(this.props.location.pathname + this.props.location.search);
    };

    render() {

        return (
            <Fragment>
                {(this.props.tracks.length > 0) ? <h2>"{this.props.info.album}"</h2> : null}
                <CardDeck>
                    {this.props.tracks.length > 0 ?
                        this.props.tracks.map(item => {
                            return <Col sm={12} key={item.name}>
                                <CardItem
                                    title={item.name}
                                    subtitle={item.duration}
                                    style={{cursor: 'pointer'}}
                                    click={() => this.clickTrack(item._id)}
                                />
                            </Col>
                        })
                        : <h3>Nothing here yet</h3>}
                </CardDeck>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        tracks: state.tracks.tracks,
        info: state.tracks.info,
        user: state.users.user,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchTrackList: query => dispatch(fetchTrackList(query)),
        postTrackToHistory: (track, token) => dispatch(postTrackToHistory(track, token))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackList);