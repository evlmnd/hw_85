import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const ADD_NEW_ALBUM_SUCCESS = 'ADD_NEW_ALBUM_SUCCESS';
export const ADD_NEW_ALBUM_FAILURE = 'ADD_NEW_ALBUM_FAILURE';
export const GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS = 'GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS';
export const GET_ALBUMS_LIST_FOR_ADMIN_FAILURE = 'GET_ALBUMS_LIST_FOR_ADMIN_FAILURE';
export const GET_MY_ALBUMS_LIST_SUCCESS = 'GET_MY_ALBUMS_LIST_SUCCESS';
export const GET_MY_ALBUMS_LIST_FAILURE = 'GET_MY_ALBUMS_LIST_FAILURE';

export const fetchAlbumsSuccess = data => ({type: FETCH_ALBUMS_SUCCESS, data});

export const fetchAlbumsList = query => {
    return dispatch => {
        return axios.get(query).then(response => {
            console.log(response.data);
            dispatch(fetchAlbumsSuccess(response.data));
            }
        )
    };
};

const addAlbumSuccess = data => ({type: ADD_NEW_ALBUM_SUCCESS, data});
const addAlbumFailure = error => ({type: ADD_NEW_ALBUM_FAILURE, error});

export const addAlbum = albumData => {
    return (dispatch, getState) => {
        const headers = {
            Token: getState().users.user.user.token
        };
        return axios.post('/albums', albumData, { headers }).then(
            response => {
                dispatch(addAlbumSuccess(response.data));
                dispatch(push('/'));
            },
            error => {
                dispatch(addAlbumFailure(error));
            }
        );
    };
};

const getAlbumsListForAdminSuccess = albums => ({type: GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS, albums});
const getAlbumsListForAdminFailure = error => ({type: GET_ALBUMS_LIST_FOR_ADMIN_FAILURE, error});

export const getAlbumsListForAdmin = () => {
    return (dispatch, getState) => {
        const headers = { Token: getState().users.user.user.token };
        return axios.get('/albums/admin', {headers}).then(
            response => {
                dispatch(getAlbumsListForAdminSuccess(response.data));
            },
            error => {
                dispatch(getAlbumsListForAdminFailure(error));
            }
        );
    };
};

const getMyAlbumsListSuccess = data => ({type: GET_MY_ALBUMS_LIST_SUCCESS, data});
const getMyAlbumsListFailure = error => ({type: GET_MY_ALBUMS_LIST_FAILURE, error});

export const getMyAlbumsList = userId => (dispatch) => {
    return axios.get(`/albums/${userId}`).then(
        response => {
            dispatch(getMyAlbumsListSuccess(response.data))
        },
        error => {
            dispatch(getMyAlbumsListFailure(error))
        }
    );
};