import axios from '../../axios-api';
import {push} from 'connected-react-router';

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const ADD_NEW_ARTIST_SUCCESS = 'ADD_NEW_ARTIST_SUCCESS';
export const ADD_NEW_ARTIST_FAILURE = 'ADD_NEW_ARTIST_FAILURE';
export const GET_ARTIST_LIST_FOR_ADMIN_SUCCESS = 'GET_ARTIST_LIST_FOR_ADMIN_SUCCESS';
export const GET_ARTIST_LIST_FOR_ADMIN_FAILURE = 'GET_ARTIST_LIST_FOR_ADMIN_FAILURE';
export const GET_MY_ARTISTS_LIST_SUCCESS = 'GET_MY_ARTISTS_LIST_SUCCESS';
export const GET_MY_ARTISTS_LIST_FAILURE = 'GET_MY_ARTISTS_LIST_FAILURE';

export const fetchArtistsSuccess = data => ({type: FETCH_ARTISTS_SUCCESS, data});

export const fetchArtistsList = () => {
    return dispatch => {
        return axios.get('/artists').then(response => {
            dispatch(fetchArtistsSuccess(response.data))
            }
        )
    };
};

const addArtistSuccess = data => ({type: ADD_NEW_ARTIST_SUCCESS, data});
const addArtistFailure = error => ({type: ADD_NEW_ARTIST_FAILURE, error});

export const addArtist = artistData => {
    return (dispatch, getState) => {
        const headers = {
            Token: getState().users.user.user.token
        };
        return axios.post("/artists", artistData, { headers }).then(
            response => {
                dispatch(addArtistSuccess(response.data));
                dispatch(push('/'));
            },
            error => {
                dispatch(addArtistFailure(error));
            }
        );
    };
};

const getArtistsListForAdminSuccess = data => ({type: GET_ARTIST_LIST_FOR_ADMIN_SUCCESS, data});
const getArtistListForAdminFailure = error => ({type: GET_ARTIST_LIST_FOR_ADMIN_FAILURE, error});

export const getArtistsListForAdmin = () => {
    return (dispatch, getState) => {
        const headers = { Token: getState().users.user.user.token };
        return axios.get('/artists/admin', {headers}).then(
            response => {
                dispatch(getArtistsListForAdminSuccess(response.data));
            },
            error => {
                dispatch(getArtistListForAdminFailure(error));
            }
        );
    };
};

const getMyArtistsListSuccess = data => ({type: GET_MY_ARTISTS_LIST_SUCCESS, data});
const getMyArtistsListFailure = error => ({type: GET_MY_ARTISTS_LIST_FAILURE, error});

export const getMyArtistsList = userId => (dispatch, getState) => {
    const headers = { Token: getState().users.user.user.token };
    return axios.get(`/artists/${userId}`, { headers }).then(
        response => {
            dispatch(getMyArtistsListSuccess(response.data));
        },
        error => {
            dispatch(getMyArtistsListFailure(error));
        }
    );
};



