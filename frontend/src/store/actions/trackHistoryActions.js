import axios from "../../axios-api";

export const POST_TRACK_TO_HISTORY_FAILURE = 'POST_TRACK_TO_HISTORY_FAILURE';
export const TRACKS_HISTORY_SUCCESS = 'TRACKS_HISTORY_SUCCESS';
export const TRACKS_HISTORY_FAILURE = 'TRACKS_HISTORY_FAILURE';

const postTrackToHistoryFailure = error => {
    return {type: POST_TRACK_TO_HISTORY_FAILURE, error};
};

export const postTrackToHistory = (track, token) => {
    return dispatch => {
        axios.post('/track_history', {track: track}, {headers: {'Token': token}} ).then(
            response => {},
            error => {
                dispatch(postTrackToHistoryFailure(error))
            }
        );
    };
};

const tracksHistorySuccess = history => {
    return {type: TRACKS_HISTORY_SUCCESS, history};
};

const tracksHistoryFailure = error => {
    return {type: TRACKS_HISTORY_FAILURE, error};
};

export const getTrackHistory = token => {
    return dispatch => {
        axios.get(`/track_history`, {headers: {'Token': token}}).then(
            response => {
                console.log(response);
                dispatch(tracksHistorySuccess(response.data))
            },
            error => {
                dispatch(tracksHistoryFailure(error))
            }
        );
    }
};
