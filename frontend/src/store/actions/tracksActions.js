import axios from '../../axios-api';
import {push} from 'connected-react-router';


export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const ADD_NEW_TRACK_SUCCESS = 'ADD_NEW_TRACK_SUCCESS';
export const ADD_NEW_TRACK_FAILURE = 'ADD_NEW_TRACK_FAILURE';

export const fetchTracksSuccess = data => ({type: FETCH_TRACKS_SUCCESS, data});

export const fetchTrackList = query => {
    return dispatch => {
        return axios.get(query).then(response => {
                dispatch(fetchTracksSuccess(response.data));
            }
        )
    };
};

const addTrackSuccess = data => ({type: ADD_NEW_TRACK_SUCCESS, data});
const addTrackFailure = error => ({type: ADD_NEW_TRACK_FAILURE, error});

export const addTrack = trackData => {
    return (dispatch, getState) => {
        const headers = {
            Token: getState().users.user.user.token
        };

        for (var pair of trackData.entries()) {
            console.log(pair[0]+ ': ' + pair[1]);
        }

        return axios.post('/tracks', trackData, { headers }).then(
            response => {
                dispatch(addTrackSuccess(response.data));
                dispatch(push('/'));
            },
            error => {
                console.log(error);
                dispatch(addTrackFailure(error));
            }
        );
    };
};