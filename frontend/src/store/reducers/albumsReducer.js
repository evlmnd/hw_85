import {
    FETCH_ALBUMS_SUCCESS, GET_ALBUMS_LIST_FOR_ADMIN_FAILURE,
    GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS,
    GET_MY_ALBUMS_LIST_FAILURE,
    GET_MY_ALBUMS_LIST_SUCCESS
} from "../actions/albumsActions";

const initialState = {
    albums: [],
    name: '',
    error: null
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_SUCCESS:
            if (action.data.length > 0) {
                return {...state, albums: action.data, name: action.data[0].artist.name, error: null};
            } else {
                return {...state}
            }
        case GET_MY_ALBUMS_LIST_SUCCESS:
            return {...state, albums: action.data};
        case GET_MY_ALBUMS_LIST_FAILURE:
            return {...state, error: action.error};
        case GET_ALBUMS_LIST_FOR_ADMIN_SUCCESS:
            return {...state, albums: action.albums};
        case GET_ALBUMS_LIST_FOR_ADMIN_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default albumsReducer;