import {
    ADD_NEW_ARTIST_FAILURE,
    FETCH_ARTISTS_SUCCESS, GET_ARTIST_LIST_FOR_ADMIN_FAILURE, GET_ARTIST_LIST_FOR_ADMIN_SUCCESS,
    GET_MY_ARTISTS_LIST_FAILURE,
    GET_MY_ARTISTS_LIST_SUCCESS
} from "../actions/artistsActions";

const initialState = {
    artists: [],
    error: null
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.data, error: null};
        case ADD_NEW_ARTIST_FAILURE:
            return { ...state, error: action.error };
        case GET_MY_ARTISTS_LIST_SUCCESS:
            return { ...state, artists: action.data };
        case GET_MY_ARTISTS_LIST_FAILURE:
            return { ...state, error: action.error };
        case GET_ARTIST_LIST_FOR_ADMIN_SUCCESS:
            return { ...state, artists: action.data };
        case GET_ARTIST_LIST_FOR_ADMIN_FAILURE:
            return { ...state, error: action.error };
        default:
            return state;
    }
};

export default artistsReducer;