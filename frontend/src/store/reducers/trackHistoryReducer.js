import {
    POST_TRACK_TO_HISTORY_FAILURE,
    TRACKS_HISTORY_FAILURE,
    TRACKS_HISTORY_SUCCESS
} from "../actions/trackHistoryActions";

const initialState = {
    error: null,
    history: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_TRACK_TO_HISTORY_FAILURE:
            return {...state, error: action.error};
        case TRACKS_HISTORY_SUCCESS:
            return {...state, history: action.history};
        case TRACKS_HISTORY_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default reducer;