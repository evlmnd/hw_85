import {FETCH_TRACKS_SUCCESS} from "../actions/tracksActions";

const initialState = {
    tracks: [],
    info: {},
    error: null
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_SUCCESS:
            if (action.data.length > 0) {
                return {...state, tracks: action.data, info: {album: action.data[0].album.name}, error: null};
            } else {
                return {...state}
            }
        default:
            return state;
    }
};

export default artistsReducer;